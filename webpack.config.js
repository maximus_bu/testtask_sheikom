const path = require('path');

module.exports = {
    mode: "production",
    entry: "./src/app",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "app.js",
    },
    devtool: "source-map"
}
