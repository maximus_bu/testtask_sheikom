import {Chest} from "./chest";
import {playBtn} from "./playBtn";
import {WinningView} from "./winningView";
import CONST from "../utils/const";


export class View {
    constructor(ee) {
        this.emitter = ee;
        this.display = new PIXI.Container();
        this.chests = [];
        this.openedChestCnt = 0;

        this.addChests();
        this.addPlayBtn();
        this.addWinningsLatyer();
    }

    addWinningsLatyer() {
        this.winnings = new WinningView(this.emitter);

        const layerName = new PIXI.Text('Main game Screen');
        layerName.anchor.set(0.5,0);
        layerName.position = new PIXI.Point(CONST.winSize.width/2,10);
        this.display.addChild(layerName);
        this.display.addChild(this.winnings.display);
    }

    addPlayBtn() {
        this.playBtn = new playBtn();
        this.display.addChild(this.playBtn.display);
        this.playBtn.display.on('pointerup', () => {
            this.emitter.emit(CONST.EVENTS.PLAY);
        });
    }

    addChests() {
        for (let i = 0; i < 6; i++) {
            const chest = new Chest();

            chest.display.on('pointerup', this.chestClick.bind(this,chest));

            let x = i % 2 < 1 ? CONST.winSize.width / 3 - chest.display.width : CONST.winSize.width / 3 * 2;
            //todo "magic numbers" add to constants
            let y = i%3*80 + 50;
            chest.display.position.set(x, y);
            this.display.addChild(chest.display);
            this.chests.push(chest);
        }
    }

    chestClick(chest) {
        this.emitter.emit(CONST.EVENTS.OPENCHEST);

        chest.setDisable();
        this.openedChestCnt++;
        //todo get chest count from model
        if (this.openedChestCnt === 6) {
            this.enableChests();
        }
    }

    //todo make proper animation on Main game screen
    animateWinning(winning) {
        if (winning.type === CONST.WINTYPES.LOOSE) {
            this.winnings.showLoose()
        } else {
            this.winnings.showWin(winning)
        }
    }

    enableChests() {
        this.chests.forEach((chest) => {
            chest.setEnable();
        });
        this.playBtn.setDisable();
    }


}
