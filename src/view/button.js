export class Button {
    constructor() {
        this.display = new PIXI.Graphics();
        this.createButtonInstance()
    }

    createButtonInstance() {
    }

    setEnable() {
        this.display.buttonMode = true;
        this.display.interactive = true;
        this.display.alpha = 1;
    }

    setDisable() {
        this.display.buttonMode = true;
        this.display.interactive = false;
        this.display.alpha = 0.5;
    }


 }
