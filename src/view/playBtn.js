import {Button} from "./button";
import CONST from "../utils/const";

export class playBtn extends Button {

    createButtonInstance() {
        super.createButtonInstance();

        this.display.beginFill(0xDE3249);
        this.display.drawRect(0, 0, 140, 60);
        this.display.endFill();
        this.display.text = new PIXI.Text('PLAY');
        this.display.text.x = 40;
        this.display.text.y = 15;
        this.display.addChild(this.display.text);
        this.display.position = new PIXI.Point(CONST.winSize.width/2 - this.display.width/2,450);

        this.setEnable();
    }

}
