import {Button} from "./button";

export class Chest extends Button {

    createButtonInstance() {
        super.createButtonInstance();
        this.display.lineStyle(2, 0xFF00FF, 1);
        this.display.beginFill(0xFFFFFF);
        this.display.drawRect(0, 0, 100, 50);
        this.display.endFill();
        this.display.text = new PIXI.Text('Chest',{fontSize: 24});
        this.display.text.x = 20;
        this.display.text.y = 10;
        this.display.addChild(this.display.text);
        this.setDisable();
    }
}
