import CONST from "../utils/const";

export class WinningView {
    constructor(ee) {
        this.emitter = ee;
        this.display = new PIXI.Container();
        this.display.interactive = true;
        this.createWiiningLayer();
        this.hide();
    }

    createWiiningLayer() {
        const layerName = new PIXI.Text('Bonus Screen');
        layerName.anchor.set(0.5,0);
        layerName.position = new PIXI.Point(CONST.winSize.width/2,10);
        const layer = new PIXI.Graphics();
        layer.lineStyle(2, 0xFEEB77, 1);
        layer.beginFill(0x650A5A);
        layer.drawRect(0, 0, CONST.winSize.width, CONST.winSize.height);
        layer.endFill();

        this.amount = new PIXI.Text();
        this.winAnimation = new PIXI.Text();

        this.display.addChild(layer);
        this.display.addChild(layerName);
        this.display.addChild(this.winAnimation);
        this.display.addChild(this.amount);
    }

    showWin(winnings) {
        this.amount.text = "Win amount " + winnings.amount;
        //todo magic numbers
        this.amount.position = new PIXI.Point(CONST.winSize.width/2,CONST.winSize.height/2);
        this.amount.visible = true;
        this.amount.anchor.set(.5,.5);
        this.winAnimation.text = 'You Win bonus animation';
        this.playAnimation(this.winAnimation);
    }

    showLoose() {
        this.amount.text = "Try again!";
        this.playAnimation(this.amount)
    }

    playAnimation(animation) {
        animation.visible = true;
        animation.scale.set(1,1);
        animation.anchor.set(.5,.5);
        animation.position = new PIXI.Point(CONST.winSize.width/2,CONST.winSize.height/2 - 100);
        gsap.to(animation, {
            duration: 3,
            pixi: {
                scaleX: 2,
                scaleY: 2,
            },
            onComplete: this.hide.bind(this)
        });
        this.display.visible = true
    }

    hide() {
        this.winAnimation.visible = false;
        this.amount.visible = false;
        this.winAnimation.scale.set(1,1);
        this.amount.scale.set(1,1);
        this.display.visible = false
    }
}
