export class Model {
    constructor() {
        this.rows = 3;
        this.columns = 2;
        this.chestNums = 6;
        this.winRegular = {
            weight: 0.5,
            amount: 500,
            type: 'normal'
        };
        this.winBonus = {
            weight: 0.2,
            amount: 1000,
            type: 'bonus'
        };
        this.loose = {
            weight: 0,
            amount: 0,
            type: 'loose'
        }
    }

    generateWinning(){
        const random = Math.random().toFixed(1);
        console.log(random);
        switch (true) {
            case random <= this.winBonus.weight :
                return this.winBonus;
            case random <= this.winRegular.weight:
                return this.winRegular;
            default : return this.loose
        }
    }
}
