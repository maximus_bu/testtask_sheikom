const CONST = {
    EVENTS: {
        PLAY: "play",
        OPENCHEST: "openChest"
    },
    WINTYPES: {
        NORMAL: "normal",
        BONUS: "bonus",
        LOOSE: "loose"
    },
    winSize: {
        width : 800,
        height: 600
    }
}

export default CONST
