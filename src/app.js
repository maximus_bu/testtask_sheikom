
import {Controller} from "./controller/controller";
import {EventEmitter} from "events";
import {View} from "./view/view";
import {Model} from "./model/model";
import CONST from "./utils/const";

const app = new PIXI.Application({
    width: CONST.winSize.width, height: CONST.winSize.height, backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1,
});
document.body.appendChild(app.view);

const ee = new EventEmitter();
const view = new View(ee);
const model = new Model(ee);
const contr = new Controller(view,model);


app.stage.addChild(view.display);

