import CONST from "../utils/const";

export class Controller {
    constructor(view,model) {
        this.view = view;
        this.model = model;

        this.view.emitter.on(CONST.EVENTS.PLAY, ()=>{
            this.view.enableChests();
        });

        this.view.emitter.on(CONST.EVENTS.OPENCHEST, ()=>{
            const winning = this.model.generateWinning();
            this.view.animateWinning(winning)
        });

    }
}
